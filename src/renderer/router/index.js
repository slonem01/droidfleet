import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/Splash',
      name: 'splash-page',
      component: require('@/components/SplashPage').default,
      children: [
        {
          path: '',
          name: 'dash-board',
          component: require('@/components/DashBoard').default
        },
        {
          path: 'DeviceManager',
          name: 'device-manager',
          component: require('@/components/DeviceManager').default
        },
        {
          path: 'DeviceProfiles',
          name: 'device-profiles',
          component: require('@/components/DeviceProfiles').default
        },
        {
          path: 'AppSettings',
          name: 'app-settings',
          component: require('@/components/AppSettings').default
        }
      ]
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
